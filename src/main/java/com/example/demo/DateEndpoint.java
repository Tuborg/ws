package com.example.demo;

import com.example.demo.date_ws.GetDateResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Date;
import java.util.GregorianCalendar;

@Endpoint
public class DateEndpoint {
	private static final String NAMESPACE_URI = "http://example.com/demo/date-ws";
	private static DatatypeFactory df = null;
	static {
		try {
			df = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException dce) {
			throw new IllegalStateException(
					"Exception while obtaining DatatypeFactory instance", dce);
		}
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getDateRequest")
	@ResponsePayload
	public GetDateResponse getDate() {
		GetDateResponse response = new GetDateResponse();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis(new Date().getTime());
		response.setDate(df.newXMLGregorianCalendar(gc));
		return response;
	}
}
